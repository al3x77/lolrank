//
//  NewsViewController.m
//  LolRnk
//
//  Created by Marc-Alexandre GHALY on 07/09/15.
//  Copyright © 2015 Marc-Alexandre GHALY. All rights reserved.
//

#import "NewsViewController.h"

@interface NewsViewController ()

@end

@implementation NewsViewController
@synthesize newsBtn, championsBtn;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIImage *btnImageNews = [UIImage imageNamed:@"news.png"];
    UIImage *btnImageChampions = [UIImage imageNamed:@"champions.png"];
    
    [newsBtn setImage:btnImageNews forState:UIControlStateNormal];
    [championsBtn setImage:btnImageChampions forState:UIControlStateNormal];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
