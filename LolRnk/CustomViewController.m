//
//  CustomViewController.m
//  LolRnk
//
//  Created by Marc-Alexandre GHALY on 11/09/14.
//  Copyright (c) 2014 Marc-Alexandre GHALY. All rights reserved.
//

#import "CustomViewController.h"
#import "AFNetworking.h"


@interface CustomViewController ()
{
    BOOL _bannerIsVisible;
    ADBannerView *_adBanner;
}
@end

@implementation CustomViewController

@synthesize SummonerName, SummonerTier5v5, SummonerLP5v5, Data, Dataa, TierImage, playedGames, victories, losses;

- (void)viewDidLoad {
    
    [self performSelector:@selector(mymethod) withObject:nil afterDelay:2.0];
    [self loadData];
  
   // self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"katarinafont.jpg"]];
    
    /*
    animatedImage.animationImages = [NSArray arrayWithObjects:[UIImage imageNamed:@"carréL1_2.png"],
                                     [UIImage imageNamed:@"carréL2_2.png"],
                                     [UIImage imageNamed:@"carréL3_2.png"],
                                     [UIImage imageNamed:@"carréL1.png"],
                                     [UIImage imageNamed:@"carréL2.png"],
                                     [UIImage imageNamed:@"carréL3.png"],
                                     nil];
    
    [animatedImage setAnimationRepeatCount:0];
    
    animatedImage.animationDuration = 4.3;
    [self.view insertSubview:animatedImage aboveSubview:self.view];
    [animatedImage startAnimating];
    */

    [super viewDidLoad];
}

- (void)mymethod {
    [indicator stopAnimating];
}

- (void) loadData {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
   
    NSString *Username = [defaults objectForKey:@"inputSaved"];    
    
    NSString *ServerToLow = [defaults objectForKey:@"server"];
    NSString *Server = [ServerToLow lowercaseString];
    
    NSString *urlServerStart = @"https://";
    NSString *UrlServerMid = @".api.pvp.net/api/lol/";
    NSString *UrlScnServerMid = @"/v1.4/summoner/by-name/";
    NSString *apiKey = @"?api_key=b9b7fd33-48b1-4c56-92d8-b922aaece1be";
    NSString *apiKeyy = @"api_key=b9b7fd33-48b1-4c56-92d8-b922aaece1be";
    NSString *UserNameLow = [Username lowercaseString];
    NSString *UserNameWellFormated = [UserNameLow stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    NSString *URLconcatenated = [NSString stringWithFormat:@"%@%@%@%@%@%@%@", urlServerStart, Server ,UrlServerMid, Server, UrlScnServerMid, UserNameWellFormated, apiKey];

    NSString *MyUrl = [URLconcatenated stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
        /*************************/
        /*** Première Requete GET ***/
        /*************************/

        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        [manager GET:MyUrl parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {

        self.Data = (NSDictionary *)responseObject;
        
        NSString *SumId = [NSString stringWithFormat:@"%@",self.Data[UserNameWellFormated][@"id"]];
        
        NSString *sumName = self.Data[UserNameWellFormated][@"name"];
        SummonerName.text = sumName;
        SummonerName.textColor = [UIColor whiteColor];

        NSString *rnkSeason = @"/v2.5/league/by-summoner/";
        NSString *statsRnk = [NSString stringWithFormat:@"%@%@%@%@%@%@/entry%@", urlServerStart, Server, UrlServerMid, Server, rnkSeason, SumId, apiKey];
            
                /*************************/
                /*** 2eme Requete GET ***/
                /*************************/
            
                [manager GET:statsRnk parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObjectt) {
                self.Dataa = (NSDictionary *)responseObjectt;
            
                NSString *SumDiv5v5 = [NSString stringWithFormat:@"%@",self.Dataa[SumId][0][@"tier"]];
                NSString *div5v5 = [NSString stringWithFormat:@"%@", self.Dataa[SumId][0][@"entries"][0][@"division"]];
                NSString *Div5v5conc = [NSString stringWithFormat:@"%@ %@", SumDiv5v5, div5v5];
            
                NSString *SumLP5v5 = [NSString stringWithFormat:@"%@ LP", self.Dataa[SumId][0][@"entries"][0][@"leaguePoints"]];

                SummonerTier5v5.text = Div5v5conc;
                SummonerTier5v5.textColor = [UIColor colorWithRed:0.506 green:0.953 blue:0.992 alpha:1]; //light blue
                SummonerLP5v5.text = SumLP5v5;
                SummonerLP5v5.textColor = [UIColor whiteColor];
            
                /*
                NSString *wins = [NSString stringWithFormat:@"%@", self.Dataa[SumId][0][@"entries"][0][@"wins"]];
                
                victories.text = wins;
                victories.textColor = [UIColor greenColor];
                */
                
                if ([SumDiv5v5 isEqual:@"BRONZE"])
                {
                    TierImage.image = [UIImage imageNamed:@"bronze.png"];
                }
                else if ([SumDiv5v5 isEqual:@"SILVER"])
                {
                    TierImage.image = [UIImage imageNamed:@"silver.png"];
                }
                else if ([SumDiv5v5 isEqual:@"GOLD"])
                {
                    TierImage.image = [UIImage imageNamed:@"gold.png"];
                }
                else if ([SumDiv5v5 isEqual:@"PLATINUM"])
                {
                    TierImage.image = [UIImage imageNamed:@"platinum.png"];
                }
                else if ([SumDiv5v5 isEqual:@"DIAMOND"])
                {
                    TierImage.image = [UIImage imageNamed:@"diamond.png"];
                }
                else if ([SumDiv5v5 isEqual:@"MASTER"])
                {
                    TierImage.image = [UIImage imageNamed:@"master.png"];
                }
                else if ([SumDiv5v5 isEqual:@"CHALLENGER"])
                {
                    TierImage.image = [UIImage imageNamed:@"challenger.png"];
                }
                
                    /***********************/
                    /** 3eme Requete HTTP **/
                    /***********************/
                    
                    NSString *stats = @"/v1.3/stats/by-summoner/";
                    NSString *scndprt = @"/summary?season=SEASON2015&";
                
                    NSString *statsWellFormated = [NSString stringWithFormat:@"%@%@%@%@%@%@%@%@", urlServerStart, Server, UrlServerMid, Server, stats, SumId, scndprt, apiKeyy];
                
                    [manager GET:statsWellFormated parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObjecttt) {
                        
                        self.Dataaa = (NSDictionary *)responseObjecttt;
                                                
                        for (int i = 0; i < [self.Dataaa[@"playerStatSummaries"] count]; i++ )
                        {
                            NSString *verif = [NSString stringWithFormat:@"%@", self.Dataaa[@"playerStatSummaries"][i][@"playerStatSummaryType"]];
                            
                            if ([verif isEqualToString:@"RankedSolo5x5"])
                            {
                                NSString *wins = [NSString stringWithFormat:@"%@", self.Dataaa[@"playerStatSummaries"][i][@"wins"]];
                                NSString *lousses = [NSString stringWithFormat:@"%@", self.Dataaa[@"playerStatSummaries"][i][@"losses"]];
                           
                                int winValue = [wins intValue];
                                int lossValue = [lousses intValue];
                                int playedGamess = winValue + lossValue;
                                
                                NSString *RnkPlayedGames = [NSString stringWithFormat:@"%d", playedGamess];
                                
                                victories.text = wins;
                                victories.textColor = [UIColor greenColor];
                                
                                playedGames.text = RnkPlayedGames;
                                playedGames.textColor = [UIColor brownColor];
                                
                                losses.text = lousses;
                                losses.textColor = [UIColor redColor];
                            }
                        
                        }
                        

                    }
                         failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                             
                             victories.text = @"";
                             playedGames.text = @"";
                             losses.text = @"";
                         }];
                 
                }
        failure:^(AFHTTPRequestOperation *operation, NSError *error) {

            SummonerTier5v5.text = @"Player inactive in ranked 5v5";
            SummonerTier5v5.textColor = [UIColor orangeColor];
            SummonerLP5v5.text = @"";
            TierImage.image = [UIImage imageNamed:@"none.png"];
            
            victories.text = @"";
            playedGames.text = @"";
            losses.text = @"";
        }];
        
    }
     
    failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSInteger responsecode = [operation.response statusCode];
        
        if (responsecode == 503)
        {
            SummonerName.text = @"Server temporary unavailable";
            [SummonerName setFont:[UIFont systemFontOfSize:10.7]];
            [SummonerName sizeToFit];
            SummonerName.textColor = [UIColor redColor];
      
            SummonerTier5v5.text = @"Please retry later";
            SummonerLP5v5.text = @"";
            
            victories.text = @"";
            playedGames.text = @"";
            losses.text = @"";
        }
        
        if (responsecode == 404)
       {
           SummonerName.text = @"Summoner does not exist or at least is not of this region";
           [SummonerName setFont:[UIFont systemFontOfSize:10.7]];
           [SummonerName sizeToFit];
           SummonerName.textColor = [UIColor redColor];
           
           SummonerTier5v5.text = @"Please retry with another username";
           SummonerLP5v5.text = @"";
      
           victories.text = @"";
           playedGames.text = @"";
           losses.text = @"";
       }
    }];
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    
    _adBanner = [[ADBannerView alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height, 320, 50)];
    _adBanner.delegate = self;
}

-(void)bannerViewDidLoadAd:(ADBannerView *)banner {

    if (!_bannerIsVisible)
    {
        if (_adBanner.superview == nil)
        {
            [self.view addSubview:_adBanner];
        }
        
        [UIView beginAnimations:@"animateAdBannerOn" context:NULL];
     
        banner.frame = CGRectOffset(banner.frame, 0, -banner.frame.size.height);
    
        [UIView commitAnimations];
        
        _bannerIsVisible = YES;
    }
}

-(void)bannerView:(ADBannerView *)banner didFailToReceiveAdWithError:(NSError *)error {
    NSLog(@"Failed to retrieve ad");

    if (_bannerIsVisible)
    {
        [UIView beginAnimations:@"animateAdBannerOff" context:NULL];
        banner.frame = CGRectOffset(banner.frame, 0, banner.frame.size.height);
        
        [UIView commitAnimations];
        
        _bannerIsVisible = NO;
    }
        
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
