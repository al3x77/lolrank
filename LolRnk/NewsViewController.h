//
//  NewsViewController.h
//  LolRnk
//
//  Created by Marc-Alexandre GHALY on 07/09/15.
//  Copyright © 2015 Marc-Alexandre GHALY. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewsViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIButton *newsBtn;
@property (weak, nonatomic) IBOutlet UIButton *championsBtn;


@end
