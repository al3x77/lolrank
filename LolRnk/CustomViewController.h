//
//  CustomViewController.h
//  LolRnk
//
//  Created by Marc-Alexandre GHALY on 11/09/14.
//  Copyright (c) 2014 Marc-Alexandre GHALY. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <iAd/iAd.h>

@interface CustomViewController : UIViewController <ADBannerViewDelegate> {
    
    IBOutlet UIActivityIndicatorView *indicator;
}

@property (strong, nonatomic) IBOutlet UILabel *SummonerName;
@property (strong, nonatomic) IBOutlet UILabel *SummonerTier5v5;
@property (strong, nonatomic) IBOutlet UILabel *SummonerLP5v5;
@property (strong, nonatomic) IBOutlet UIImageView *TierImage;
@property (strong, nonatomic) IBOutlet UILabel *playedGames;
@property (strong, nonatomic) IBOutlet UILabel *victories;
@property (strong, nonatomic) IBOutlet UILabel *losses;

@property (strong, nonatomic) NSDictionary *Data;
@property (strong, nonatomic) NSDictionary *Dataa;
@property (strong, nonatomic) NSDictionary *Dataaa;

@end
