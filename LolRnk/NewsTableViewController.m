//
//  NewsTableViewController.m
//  LolRnk
//
//  Created by Marc-Alexandre GHALY on 10/09/15.
//  Copyright © 2015 Marc-Alexandre GHALY. All rights reserved.
//

#import "NewsTableViewController.h"

@interface NewsTableViewController ()

@end

@implementation NewsTableViewController
@synthesize titles, icons, imageCache;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    titles = [[NSMutableArray alloc] init];
    icons = [[NSMutableArray alloc] init];
    
    PFQuery *queryIcons = [PFQuery queryWithClassName:@"News"];
    PFQuery *queryTitles = [PFQuery queryWithClassName:@"News"];
    
    [queryIcons selectKeys:@[@"Images"]];
    [queryTitles selectKeys:@[@"Title"]];
    
    NSMutableArray *tempArrayIcons = [queryIcons findObjects];
    NSMutableArray *tempArrayTitles = [queryTitles findObjects];

    
    long nbr = [tempArrayTitles count];
    long i = 0;
    
    while (i < nbr) {
        
        NSString *objStrTitle = [NSString stringWithFormat:@"%@", tempArrayTitles[i][@"Title"]];
        NSString *objStrIcons = [NSString stringWithFormat:@"%@", tempArrayIcons[i][@"Images"]];
        
        /*********   transformer NSString en NSUrl ici    **********/
        NSString *webStr = [[NSString alloc] initWithFormat:@"%@", objStrIcons];
        NSURL *webURL = [[NSURL alloc] initWithString:[webStr stringByAddingPercentEscapesUsingEncoding:NSASCIIStringEncoding]];
        
        /**** ensuite addObjects ***/
        
        [icons addObject:webURL];
        
        
        if (!([objStrTitle length] == 0)) {
         [titles addObject:objStrTitle];
        }
         i++;
     }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [titles count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier =@"Cell";
    UITableViewCell *cell = [tableView
                             dequeueReusableCellWithIdentifier:CellIdentifier];
    

    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
        //Background Thread
        
        NSString *urlStr =[NSString stringWithFormat:@"%@", [icons objectAtIndex:indexPath.row]];
        NSURL *url = [NSURL URLWithString:urlStr];
        NSData *data = [NSData dataWithContentsOfURL:url];
        UIImage *image = [UIImage imageWithData:data];


        dispatch_async(dispatch_get_main_queue(), ^(void){
            //Run UIUpdates
            [cell.imageView setImage:image];
         //   cell.imageView.image = image;
            cell.textLabel.text= [titles objectAtIndex:indexPath.row];
            cell.textLabel.textColor = [UIColor whiteColor];
            

        });
    });
    

    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSLog(@"tapped");
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"ShowDetail"])
    {
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        DetailedCellViewController *destViewController = segue.destinationViewController;
        destViewController.retrievedTitle = [titles objectAtIndex:indexPath.row];
        
        NSString *urlStr =[NSString stringWithFormat:@"%@", [icons objectAtIndex:indexPath.row]];
        NSURL *url = [NSURL URLWithString:urlStr];
        NSData *data = [NSData dataWithContentsOfURL:url];
        UIImage *image = [UIImage imageWithData:data];
        
        destViewController.retrievedIterator = indexPath.row;
        destViewController.retrievedImage= image;
    }
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
