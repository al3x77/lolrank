//
//  WelcomeViewController.m
//  LolRnk
//
//  Created by Marc-Alexandre GHALY on 07/09/15.
//  Copyright © 2015 Marc-Alexandre GHALY. All rights reserved.
//

#import "WelcomeViewController.h"

@interface WelcomeViewController ()

@end

@implementation WelcomeViewController

@synthesize eSportButton, rankingButton;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIImage *btnImage = [UIImage imageNamed:@"lolworld.png"];
    UIImage *btnImageRnk = [UIImage imageNamed:@"rank_icon.png"];
    
    [rankingButton setImage:btnImageRnk forState:UIControlStateNormal];
    [eSportButton setImage:btnImage forState:UIControlStateNormal];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
