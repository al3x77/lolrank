//
//  DetailedCellViewController.m
//  LolRnk
//
//  Created by Marc-Alexandre GHALY on 11/09/15.
//  Copyright © 2015 Marc-Alexandre GHALY. All rights reserved.
//

#import "DetailedCellViewController.h"

@interface DetailedCellViewController ()

@end

@implementation DetailedCellViewController
@synthesize titledtl, imgdtl, articledtl, retrievedTitle, retrievedImage, retrievedIterator, article;

- (void)viewDidLoad {
    [super viewDidLoad];

    titledtl.text = retrievedTitle;
    imgdtl.image = retrievedImage;
    
    PFQuery *queryArticle = [PFQuery queryWithClassName:@"News"];
    [queryArticle selectKeys:@[@"Article"]];
    NSMutableArray *tempArrayIcons = [queryArticle findObjects];
 
    articledtl.text = [tempArrayIcons[retrievedIterator][@"Article"] stringByAppendingString:@"\n\n"];

    [articledtl setBackgroundColor: [UIColor clearColor]];
    [articledtl setTextColor: [UIColor whiteColor]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
