//
//  ChampionsTableViewController.h
//  LolRnk
//
//  Created by Marc-Alexandre GHALY on 11/09/15.
//  Copyright © 2015 Marc-Alexandre GHALY. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChampionsTableViewController : UITableViewController


@property (nonatomic, strong) NSMutableArray *myArr;

@end
