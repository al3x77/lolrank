//
//  NewsTableViewController.h
//  LolRnk
//
//  Created by Marc-Alexandre GHALY on 10/09/15.
//  Copyright © 2015 Marc-Alexandre GHALY. All rights reserved.
//

#import <Parse/Parse.h>
#import <UIKit/UIKit.h>
#import "DetailedCellViewController.h"

@interface NewsTableViewController : UITableViewController

@property (nonatomic,strong) NSMutableArray *titles;
@property (nonatomic, strong) NSMutableArray *icons;
@property (nonatomic, strong) NSCache *imageCache;


@end
