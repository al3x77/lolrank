//
//  ViewController.m
//  LolRnk
//
//  Created by Marc-Alexandre GHALY on 11/09/14.
//  Copyright (c) 2014 Marc-Alexandre GHALY. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

@synthesize SummonerNameTextField;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSString *appDomain = [[NSBundle mainBundle] bundleIdentifier];
    [[NSUserDefaults standardUserDefaults] removePersistentDomainForName:appDomain];
    
    self.SummonerNameTextField.delegate = self;
    
    dataArray = [[NSArray alloc]initWithObjects:@"Select region", @"EUW", @"NA", @"KR", @"EUNE", nil];
    myPicker.delegate = self;

    /***** Verification connexion internet chargement de la vue *****/
    [self testInternetConnection];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];

}

-(NSInteger)NumberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    return [dataArray count];
}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    
    NSString *server = [dataArray objectAtIndex:row];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:server forKey:@"server"];
    [defaults synchronize];
}

-(NSString *)pickerView:(UIPickerView *)pickerView attributedTitleForRow:(NSInteger)row forComponent:(NSInteger)component {
  
    NSAttributedString *roww = [[NSAttributedString alloc] initWithString:[dataArray objectAtIndex:row] attributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    return roww;
}

- (IBAction)SaveDataAndNewView:(id)sender {
    NSString *input = SummonerNameTextField.text;
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:input forKey:@"inputSaved"];
    [defaults synchronize];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [SummonerNameTextField resignFirstResponder];
    return NO;
}


/***********  Verification connexion internet   *************/


- (void) testInternetConnection
{
    
    internetReachableFoo = [Reachability reachabilityWithHostname:@"www.google.com"];
    
    // Internet is reachable
    internetReachableFoo.reachableBlock = ^(Reachability*reach)
    {
        dispatch_async(dispatch_get_main_queue(), ^{
           
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            long value = 0;
            [defaults setInteger:value forKey:@"net"];
            
            [defaults synchronize];
            NSLog(@"Yayyy, we have the interwebs!");
        });
    };
    
    
    // Internet is not reachable
    internetReachableFoo.unreachableBlock = ^(Reachability*reach)
    {
        dispatch_async(dispatch_get_main_queue(), ^{
    
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            long value = 1;
            [defaults setInteger:value forKey:@"net"];
            
            [defaults synchronize];
            
            NSLog(@"Someone broke the internet :(");
        });
    };
    

    [internetReachableFoo startNotifier];
}





/***************** Verifications intermédiaires *****************/

-(BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender {
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *server = [defaults objectForKey:@"server"];
    NSString *username = [defaults objectForKey:@"inputSaved"];
    long value = [defaults integerForKey:@"net"];
    
    NSLog(@"%ld", value);
    
    
        if (value == 1) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"No internet connection available" delegate:nil cancelButtonTitle:@"Dismiss"
                                              otherButtonTitles:nil];
            [alert show];
        
            return NO;
        }
        else if ((([server length] == 0) || ([username length] == 0) || [server isEqualToString:@"Select region"] ))
        {

            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Please specify a user and a region" delegate:nil cancelButtonTitle:@"Dismiss"
                                  otherButtonTitles:nil];
            
            [alert show];
 
            return NO;
        }
        else
            return YES;
}



@end
