//
//  NoticeViewController.m
//  LolRnk
//
//  Created by Marc-Alexandre GHALY on 16/09/14.
//  Copyright (c) 2014 Marc-Alexandre GHALY. All rights reserved.
//

#import "NoticeViewController.h"

@interface NoticeViewController ()

@end

@implementation NoticeViewController
@synthesize notice;

- (void)viewDidLoad {
    [super viewDidLoad];
  //  self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"katarinafont.jpg"]];
    
    notice.numberOfLines = 0;
    notice.text = @"LolRnK isn’t endorsed by Riot Games\nand doesn’t reflect the views or opinions of Riot Games\nor anyone officially involved in producing or managing League of Legends.\nLeague of Legends and Riot Games are trademarks\n or registered trademarks of Riot Games,\nInc.League of Legends © Riot Games, Inc.";
    notice.textColor = [UIColor whiteColor];
    [notice setTextAlignment:NSTextAlignmentCenter];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
