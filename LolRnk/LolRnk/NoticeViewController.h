//
//  NoticeViewController.h
//  LolRnk
//
//  Created by Marc-Alexandre GHALY on 16/09/14.
//  Copyright (c) 2014 Marc-Alexandre GHALY. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <iAd/iAd.h>

@interface NoticeViewController : UIViewController <ADBannerViewDelegate> {

}
@property (strong, nonatomic) IBOutlet UILabel *notice;

@end
