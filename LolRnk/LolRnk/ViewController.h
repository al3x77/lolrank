//
//  ViewController.h
//  LolRnk
//
//  Created by Marc-Alexandre GHALY on 11/09/14.
//  Copyright (c) 2014 Marc-Alexandre GHALY. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Reachability.h"

@interface ViewController : UIViewController <UIPickerViewDelegate>
{
    IBOutlet UIPickerView *myPicker;
    NSArray *dataArray;
    Reachability *internetReachableFoo;
}

@property (strong, nonatomic) IBOutlet UITextField *SummonerNameTextField;
- (IBAction)SaveDataAndNewView:(id)sender;

@end

