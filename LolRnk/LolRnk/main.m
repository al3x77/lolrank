//
//  main.m
//  LolRnk
//
//  Created by Marc-Alexandre GHALY on 11/09/14.
//  Copyright (c) 2014 Marc-Alexandre GHALY. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
