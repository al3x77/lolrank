//
//  DetailedCellViewController.h
//  LolRnk
//
//  Created by Marc-Alexandre GHALY on 11/09/15.
//  Copyright © 2015 Marc-Alexandre GHALY. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>

@interface DetailedCellViewController : UIViewController

@property (nonatomic,strong) NSMutableArray *article;

@property (nonatomic, assign) NSInteger retrievedIterator;
@property (strong, nonatomic) UIImage *retrievedImage;
@property (strong, nonatomic) NSString *retrievedTitle;

@property (weak, nonatomic) IBOutlet UILabel *titledtl;
@property (weak, nonatomic) IBOutlet UIImageView *imgdtl;
@property (weak, nonatomic) IBOutlet UITextView *articledtl;

@end
